/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import java.util.*;
import entidades.*;
import interfaz.*;
import java.time.*;

/**
 *
 * @author Brank
 */
public class Tarjetahabiente extends Persona {

    private LocalDate fechaNacimiento;
    private String ciudadResidencia;
    private ArrayList<Tarjeta> tarjetas;
    private static Tarjetahabiente usuarioActivo;

    public Tarjetahabiente(String nombreCompleto, String correo, String contraseña,
            LocalDate fechaNacimiento, String ciudadResidencia, ArrayList<Tarjeta> tarjetas) {
        super(nombreCompleto, correo, contraseña);
        this.fechaNacimiento = fechaNacimiento;
        this.ciudadResidencia = ciudadResidencia;
        this.tarjetas = tarjetas;
    }

    public static void miCuenta(Tarjetahabiente usuario) {
        usuarioActivo = (Tarjetahabiente)interfaz.Sistema.usuarioActivo; 
        boolean salir = false;
        while (!salir) {
            Util.limpiarPantalla();           
            System.out.println("================================================================================");
            System.out.println();                        
            System.out.println("1. Nombre completo\n2. Correo\n3. Contraseña\n4. Fecha de nacimiento\n5. Ciudad de residencia\n6. Atrás");
            System.out.println();
            System.out.println("================================================================================");
            System.out.print("Ingrese opción : ");
            String opcion = Util.ingresoString();
            while ((!(Util.isNumeric(opcion))) || (!(Util.isBetween(1, 6, opcion)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                opcion = Util.ingresoString();
            }
            switch (opcion) {
                case "1":
                    System.out.println("Nombre actual : " + usuarioActivo.getNombreCompleto());
                    System.out.print("Ingrese el nuevo nombre : ");
                    String nuevoNombre = Util.toTitle(Util.ingresoString());
                    usuario.setNombreCompleto(nuevoNombre);
                    System.out.println();
                    System.out.println("Nombre modificado con éxito");
                    Util.continuar();
                    break;
                case "2":
                    System.out.println("Correo actual : " + usuarioActivo.getCorreo());
                    System.out.print("Ingrese el nuevo correo : ");
                    String nuevoCorreo = Util.ingresoString().toLowerCase();
                    usuario.setCorreo(nuevoCorreo);
                    System.out.println();
                    System.out.println("Correo modificado con éxito");
                    Util.continuar();
                    break;
                case "3":
                    System.out.print("Ingrese su antigua contraseña : ");
                    String antiguaContraseña = Util.ingresoString();
                    if (antiguaContraseña.equals(usuario.getContraseña())) {
                        System.out.print("Ingrese la nueva contraseña : ");
                        String nuevaContraseña = Util.ingresoString();
                        usuario.setContraseña(nuevaContraseña);
                        System.out.println();
                        System.out.println("Contraseña modificada con éxito");
                        Util.continuar();
                        break;
                    } else {
                        System.out.println();
                        System.out.println("Contraseña incorrecta");
                        Util.continuar();
                        break;
                    }
                case "4":
                    System.out.println("Fecha de nacimiento actual : " + Util.mostrarFecha(usuarioActivo.getFechaNacimiento()));
                    System.out.print("Ingrese la nueva fecha de nacimiento (DD-MM-AAAA) : ");                    
                    LocalDate nuevaFechaNacimiento = Util.ingresoFecha();
                    while (nuevaFechaNacimiento == null){
                        System.out.print("Formato inválido. Ingrese la fecha correctamente (DD-MM-AAAA) : ");
                        nuevaFechaNacimiento = Util.ingresoFecha();
                    }
                    usuario.setFechaNacimiento(nuevaFechaNacimiento);
                    System.out.println();
                    System.out.println("Fecha de nacimiento modificada con éxito");
                    Util.continuar();
                    break;
                case "5":
                    System.out.println("Ciudad de residencia actual : " + usuarioActivo.getCiudadResidencia());
                    System.out.print("Ingrese la nueva ciudad de residencia : ");
                    String nuevaCiudad = Util.toTitle(Util.ingresoString());
                    usuario.setCiudadResidencia(nuevaCiudad);
                    System.out.println();
                    System.out.println("Ciudad de residencia modificada con éxito");
                    Util.continuar();
                    break;
                case "6":
                    salir = true;
                    break;       
            }
        }
    }

    /* GETTERS & SETTERS */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public ArrayList<Tarjeta> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(ArrayList<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }
}